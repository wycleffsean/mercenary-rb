require 'chipmunk'

CP::Vec2.class_eval do
  class << self
    alias :[] :new
  end

  def inspect
    "#<CP::Vec2 x=#{x} y=#{y} (length ~#{length.truncate(3)})>"
  end

  # enables splatting
  def to_a
    [x,y]
  end
end
