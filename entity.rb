# frozen_string_literal: true
require 'ext/chipmunk'
require 'game_manager'
require 'forwardable'

class Entity < Struct.new(:shape, :parent)
  extend Forwardable

  def_delegator :shape, :body
  def_delegators :body, :reset_forces, :apply_force
  def_delegator :body, :p, :pos
  def_delegator :body, :p=, :pos=

  class << self
    def mass(mass)
      @mass = mass
    end
    def moment_of_inertia(moi)
      @moment_of_inertia = moi
    end
    def radius(radius)
      @radius = radius
    end

    def default_shape
      mass = @mass || 1.0
      moi = @moment_of_inertia  || 1.0
      radius = @radius || 1.0
      body = CP::Body.new(mass, moi)
      CP::Shape::Circle.new(body, radius)
    end
  end

  def initialize(parent)
    super self.class.default_shape, parent
  end

  def update; end
  def draw; end

  def dimensions
    CP::Vec2[@sprite.width, @sprite.height]
  end

  def onscreen?
    Space::BB.contain_bb?(shape.bb)
  end

  def spawn(type)
    # TODO factor out 'classify' aspect
    klass = type.to_s.split('_').map(&:capitalize).join
    klass = Kernel.const_get(klass)
    entity = klass.new self
    GameManager.entities << entity
  end
end
