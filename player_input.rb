# frozen_string_literal: true
require 'game_manager'
require 'ext/chipmunk'

module PlayerInput
  def self.buttons_down?(*buttons)
    buttons.any?{|button| Gosu::button_down? button }
  end

  def self.update
    # Player movement
    mov = CP::Vec2[0,0]
    mov.x += 1 if Gosu::button_down? Gosu::KbRight
    mov.x -= 1 if Gosu::button_down? Gosu::KbLeft
    mov.y += 1 if Gosu::button_down? Gosu::KbDown
    mov.y -= 1 if Gosu::button_down? Gosu::KbUp
    not_moving = mov.length.zero?

    unless not_moving
      mov.normalize!
      GameManager.send_message :local_player, :move, mov
    end

    # Player attack
    if buttons_down? Gosu::KbLeftShift, Gosu::KbSpace
      GameManager.send_message :local_player, :fire
    end
    yield
    #GameManager.send_message :local_player, :move, -mov unless not_moving
  end
end
