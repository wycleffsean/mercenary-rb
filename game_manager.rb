# frozen_string_literal: true
require 'chipmunk'
require 'forwardable'
require 'singleton'

class GameManager
  include Singleton

  class << self
    extend Forwardable
    def_delegator :instance, :entities

    def send_message(target, message, *args)
      instance.messages.push [target, message, args] 
    end

    def update(dt)
      instance.pass_messages
      instance.entities.each(&:update)
      instance.space.step(0.016666667) # 60hz, ignore dt for now
    end

    def draw
      instance.entities.each(&:draw)
    end
  end

  def entities
    @entities ||= EntityCollection.new(space)
  end

  def messages
    @messages ||= []
  end

  def pass_messages(&block)
    messages.each do |target, message, args|
      entities[target].public_send(message, *args)
    end
    @messages.clear # HACK
  end

  def space
    @space ||= CP::Space.new.tap do |config|
      config.damping = 0.9 # ambient friction
    end
  end

  private

  class EntityCollection
    extend Forwardable

    def_delegators :@entities, :[], :length

    def initialize(space)
      @space = space
      @entities = {}
    end

    def <<(entity)
      if entity.kind_of?(Array)
        @entities[entity[0]] = entity[1]
        @space.add_shape(entity[1].shape)
        @space.add_body(entity[1].body)
      else
        @entities[entity.object_id] = entity
        @space.add_shape(entity.shape)
        @space.add_body(entity.body)
      end
    end

    def delete(entity)
      @space.remove_body(entity.body)
      @space.remove_shape(entity.shape)
      if @entities.key?(entity.object_id)
        @entities.delete(entity.object_id)
      else
        @entities.delete_if {|_,v| v == entity }
      end
    end

    def each(&block)
      @entities.values.each(&block)
    end
  end
end

