# frozen_string_literal: true
$: << File.dirname(__FILE__)
require 'gosu'
require 'ext/chipmunk'
require 'player'
require 'blue_laser'
require 'player_input'
require 'game_manager'
require 'space'

class Window < Gosu::Window
  def initialize
    super(*Space::DIMENSIONS, false)
    self.caption = 'Mercenary'
    GameManager.entities << [:local_player, Player.new]
    @background_image = Gosu::Image.new('assets/space.bmp', tileable: true)
    @font = Gosu::Font.new(20)
  end

  def update
    @last_tick ||= Gosu::milliseconds
    dt = Gosu::milliseconds - @last_tick
    @last_tick = Gosu::milliseconds
    PlayerInput.update do
      GameManager.update(dt)
    end
    exit if Gosu::button_down? Gosu::KbQ
  end

  def draw
    GameManager.draw
    @background_image.draw(0,0,0)
    @font.draw("FPS: #{Gosu.fps}", 10, 10, 99, 1.0, 1.0, 0xff_ffff00)
    @font.draw("entities: #{GameManager.entities.length}", 10, 35, 99, 1.0, 1.0, 0xff_ffff00)
    @font.draw("bodies: #{GameManager.instance.space.instance_variable_get('@bodies').length}", 10, 60, 99, 1.0, 1.0, 0xff_ffff00)
    @font.draw("shapes: #{GameManager.instance.space.instance_variable_get('@active_shapes').length}", 10, 85, 99, 1.0, 1.0, 0xff_ffff00)
  end
end

$font = Gosu::Font.new(10)
$window = Window.new
$window.show
