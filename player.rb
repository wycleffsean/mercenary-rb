# frozen_string_literal: true
require 'entity'
require 'ext/chipmunk'
require 'space'

class Player < Entity
  mass 100.0
  radius 50.0

  def initialize
    super nil
    @sprite = Gosu::Image.new('assets/player.png')
    self.pos = CP::Vec2[100,100]
    @speed = 1_000
    @font = Gosu::Font.new(10)
  end

  def draw
    @sprite.draw_rot(*pos, 1, body.angle)
    @font.draw("force: #{shape.body.force}", *pos, 99, 1.0, 1.0, 0xff_ffff00)
  end

  def move(vec)
    body.apply_impulse vec * @speed, vec
  end

  def update
    self.pos = pos_bb.clamp_vect(self.pos)
  end

  def fire
    rate = 1 / 3
    if @next_fire.nil? || @next_fire < Gosu::milliseconds
      @next_fire = Gosu::milliseconds + rate
      spawn :blue_laser
    end
  end

  private

  def pos_bb
    @pos_bb ||= CP::BB.new(0,0,*(Space::MAX - dimensions))
  end
end

=begin
module Timing
  def rate_limit(rate, &block)
    1000 / rate
    (Gosu::milliseconds / 1000) % rate
  end
end
=end
