# frozen_string_literal: true
require 'ext/chipmunk'

class Space < CP::Space
  DIMENSIONS = [640*2,480*2].freeze
  MAX = CP::Vec2[*DIMENSIONS].freeze
  BB = CP::BB.new(0,0, *DIMENSIONS).freeze
end
