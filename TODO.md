Game State
  - type GameState = Event
  - Built from Events
  - Events are monoids
    - acts like WAL
    - members must be monoids too!
  - Set like interface
    - diff
    - merge (monoid)

Snapshots
  - Client GameState will diverge from Server GameState
  - must be able to rewind, rerun physics
    - kinda like blockchain?
  - we want to send events across network (not entities - pos, vel, forces, etc)
  - what to do about events with randomness?
    - ship seed
