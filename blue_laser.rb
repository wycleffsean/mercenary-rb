# frozen_string_literal: true
require 'entity'
require 'game_manager'
require 'space'

class BlueLaser < Entity
  mass 10.0
  radius 10.0
  def initialize(*args)
    super(*args)
    @sprite = Gosu::Image.new('assets/blue_laser.bmp')
    self.pos = (parent.pos + parent.dimensions * 0.5) \
      - (dimensions * 0.5)
    self.shape.body.v = CP::Vec2[0,-100]
    body.apply_impulse CP::Vec2[10,10], CP::Vec2[rand(100),rand(100)]
  end

  def update
    GameManager.entities.delete(self) unless onscreen?
  end

  def draw
    @sprite.draw_rot(*pos, 1, body.angle)
    $font.draw("force: #{body.rot}", *pos, 99, 1.0, 1.0, 0xff_ffff00)
  end
end
